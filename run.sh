#! /bin/bash

service mysql start
mysql < /var/www/html/initdb.sql
mysql -e "CREATE USER 'viet'@'localhost' IDENTIFIED BY '123456'"
mysql -e "GRANT ALL PRIVILEGES ON * . * TO 'viet'@'localhost'"

service apache2 start

tail -f /var/log/apache2/access.log