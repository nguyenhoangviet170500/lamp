FROM ubuntu:18.04

ENV DEBIAN_FRONTEND=noninteractive 

RUN apt update && apt upgrade -y
RUN apt install apache2 mysql-server php libapache2-mod-php php-mysql -y
RUN apt install curl htop vim -y

ADD . /var/www/html/
ADD ./config/httpd/dir.conf /etc/apache2/mods-available/dir.conf

WORKDIR /var/www/html
RUN chmod 777 ./run.sh
EXPOSE 80

CMD [ "./run.sh" ]
#ENTRYPOINT [ "tail -f /var/log/apache2/access.log" ]

